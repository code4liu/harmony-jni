




# 编译静态库
#   -q 表示想已经存在的静态文件追加 .o 文件
#
$(lib_static):
	echo "static "    
	$(if $(KBUILD_VERBOSE:1=),@) $(AR) $(ARFLAGS) $@  built-in.o
	@ # 更新静态库的符号索引表 
	$(if $(KBUILD_VERBOSE:1=),@) $(RANLIB) $@

# 编译动态库
$(lib_dynamic):
	echo "dynamic  "    
	$(if $(KBUILD_VERBOSE:1=), @)  $(CC) $(CFLAGS) -o  $@  built-in.o $(LD_PATH) $(LD_LIB)

# 编译程序: 不能添加 -shared -fPIC
$(program):
	$(if $(KBUILD_VERBOSE:1=), @) $(CC) -o $(program) $(OBJFLAGS) $(IFLAGS) built-in.o $(LD_PATH) $(LD_LIB)
