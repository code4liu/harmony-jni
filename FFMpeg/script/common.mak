####################################################################
#   参考 ffmpeg 制作
#
#
#
#####################################################################





# 定义编译工具链
define COMPILE
        $(call $(1)DEP,$(1))
        $($(1)) $($(1)FLAGS) $($(2)) $($(1)_DEPFLAGS) $($(1)_C) $($(1)_O) $(patsubst $(SRC_PATH)/%,$(SRC_LINK)/%,$<)
endef
define COMPILE_C
        $(if $(KBUILD_VERBOSE:0=), @echo $(notdir $(<:.c=.o))) 
        $(if $(KBUILD_VERBOSE:1=), @)  $(CC) $(CFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<
endef
define COMPILE_CPP
        $(if $(KBUILD_VERBOSE:0=), @echo $(notdir $(<:.c=.o))) 
        $(if $(KBUILD_VERBOSE:1=), @)  $(CPP) $(CFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<
endef
define COMPILE_S
        @# 打印
        $(if $(KBUILD_VERBOSE:0=), @echo $(notdir $(<:.c=.o))) 
        $(if $(KBUILD_VERBOSE:1=), @)  $(CPP) $(CFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<
endef


CALL_COMPILE_C   = $(call COMPILE_C)
CALL_COMPILE_CPP = $(call COMPILE_CPP)
CALL_COMPILE_S   = $(call COMPILE_S)


# 实体依赖
dep_file = .$@.d
%.o : %.c
	$(if $(KBUILD_VERBOSE:0=), @echo                    $(notdir $(<:.c=.o))) 
	$(if $(KBUILD_VERBOSE:1=), @) $(CC)  $(CFLAGS) $(IFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<

%.o : %.cpp
	$(if $(KBUILD_VERBOSE:0=), @echo                    $(notdir $(<:.c=.o))) 
	$(if $(KBUILD_VERBOSE:1=), @)  $(CPP) $(CPPFLAGS) $(IFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<

%.o : %.asm
	$(if $(KBUILD_VERBOSE:0=), @echo                    $(notdir $(<:.c=.o))) 
	$(if $(KBUILD_VERBOSE:1=), @)  $(CPP) $(CPPFLAGS) $(IFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<


%.s : %.c
	$(if $(KBUILD_VERBOSE:0=), @echo                    $(notdir $(<:.c=.o))) 
	$(if $(KBUILD_VERBOSE:1=), @)  $(CPP) $(CFLAGS) $(IFLAGS) -Wp,-MD,$(dep_file) -c -o $@ $<
