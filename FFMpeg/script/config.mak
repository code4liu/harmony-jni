
LD    = ld
CC    = gcc
CPP   = g++
AR    = ar
STRIP = strip
RANLIB = ranlib

CFLAGS   = -shared -fPIC -g -Wall
CPPFLAGS = 
# 链接参数: -shared -fPIC 动态链接则实际并没有成为一个整体，而是需要重新定位
LDFLAGS  =  -r
ARFLAGS  = -qcv
# 头文件
IFLAGS   = -I$(TOPDIR)/ffmpeg
# 程序编译参数
OBJFLAGS = -g -Wall

ifeq ($(TARGET), harmony) 
LD     = ld.lld
CC     = clang
CPP    = clang++
AR     = llvm-ar
STRIP  = llvm-strip
RANLIB = llvm-ranlib

CFLAGS   = --target=aarch64-linux-ohos \
            --gcc-toolchain=D:/Huawei/SDK/native/2.1.0.5/llvm \
            --sysroot=D:/Huawei/SDK/native/2.1.0.5/sysroot \
            -shared -fPIC -g -Wall \
            --rtlib=compiler-rt -fuse-ld=lld -Wl,--build-id=sha1 \
            -Wl,--warn-shared-textrel -Wl,--fatal-warnings -lunwind \
            -Wl,--no-undefined -Qunused-arguments -Wl,-z,noexecstack
CPPFLAGS = 
ARFLAGS  = -rcs
# 链接参数: -shared -fPIC 动态链接则实际并没有成为一个整体，而是需要重新定位
LDFLAGS  =  -r
# 头文件
IFLAGS   = -I$(TOPDIR)/ffmpeg
# 程序编译参数
OBJFLAGS = --target=aarch64-linux-ohos \
            --gcc-toolchain=D:/Huawei/SDK/native/2.1.0.5/llvm \
            --sysroot=D:/Huawei/SDK/native/2.1.0.5/sysroot \
            --rtlib=compiler-rt -fuse-ld=lld -Wl,--build-id=sha1 \
            -Wl,--warn-shared-textrel -Wl,--fatal-warnings -lunwind \
            -Wl,--no-undefined -Qunused-arguments -Wl,-z,noexecstack \
            -g -Wall


# ifeq ($(TARGET), harmony) 
endif 