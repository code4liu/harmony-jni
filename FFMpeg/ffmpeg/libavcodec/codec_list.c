static const AVCodec * const codec_list[] = {
    &ff_mjpeg_encoder,
    &ff_mjpeg_decoder,
    &ff_png_encoder,
    &ff_png_decoder,
    NULL };
