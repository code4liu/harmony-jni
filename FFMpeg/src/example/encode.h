
// ffmpegdemo.cpp : 定义控制台应用程序的入口点。
//
#ifndef __ENCODE_H__
#define __ENCODE_H__

#include <stdio.h>
 
#define __STDC_CONSTANT_MACROS
 
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#ifdef __cplusplus
};
#endif
 
 
typedef struct 
{
    AVFormatContext *pFormatCtx;
    AVCodecContext	*pCodecCtx;
    AVCodec			*pCodec;
}encode_t;


int encode_init(encode_t *enc, const char *out_file, AVFormatContext *iFormatCtx, AVCodecContext *iCodecCtx);

void encode_exit(encode_t *enc);

int flush_encoder(AVFormatContext *fmt_ctx, unsigned int stream_index);

#if 0
int encode(encode_t *enc);

int encode2();
#endif

#endif