
#ifndef __DECODE_H__
#define __DECODE_H__

#include <stdbool.h>
// #include <libavutil/timestamp.h>
// #include <libavformat/avformat.h>
#define __STDC_CONSTANT_MACROS
 
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#ifdef __cplusplus
};
#endif

typedef struct 
{
    AVFormatContext *pFormatCtx;
    AVCodecContext	*pCodecCtx;
    AVCodec			*pCodec;
    int              channels;
    int              channel_layout;
    int              width;
    int              height;
    enum AVPixelFormat pix_fmt;
}decode_t;


int decode_init(decode_t* dec, char *in_file);

void decode_exit(decode_t* dec);

#if 0
/**
 * @brief 将 AVPacket 解码成 AVFrame
 * 
 */
int decode(AVCodecContext *avctx, AVFrame *frame, int *got_frame, AVPacket *pkt);

decode2();

#endif

#endif
