/** 
 * 最简单的基于FFmpeg的AVFilter例子 - 纯净版
 * Simplest FFmpeg AVfilter Example - Pure
 *
 * 雷霄骅 Lei Xiaohua
 * leixiaohua1020@126.com
 * 中国传媒大学/数字电视技术
 * Communication University of China / Digital TV Technology
 * http://blog.csdn.net/leixiaohua1020
 * 
 * 本程序使用FFmpeg的AVfilter实现了YUV像素数据的滤镜处理功能。
 * 可以给YUV数据添加各种特效功能。
 * 是最简单的FFmpeg的AVFilter方面的教程。
 * 适合FFmpeg的初学者。
 *
 * This software uses FFmpeg's AVFilter to process YUV raw data.
 * It can add many excellent effect to YUV data.
 * It's the simplest example based on FFmpeg's AVFilter. 
 * Suitable for beginner of FFmpeg 
 *
 */
#ifndef __FILTER_H__
#define __FILTER_H__


#include <stdio.h>
 
#define __STDC_CONSTANT_MACROS
 
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <stdbool.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif




 typedef struct 
 {
    AVFilterGraph   *filter_graph;
	AVFilterContext *outFilterCtx;
	AVFilterContext *inFilterCtx;
	AVFilterInOut *outputs;
	AVFilterInOut *inputs;
	AVFrame *inFrame;
	AVFrame *outFrame;
 }filter_t;
 

int filter_init(filter_t *fil, const char *filter_descr, int width, int height, int format, int channel_layout, int channels);

void filter_exit(filter_t *filter);

int filter(filter_t *fil, AVFrame *inFrame, AVFrame *outFrame);

AVFrame *frame_init(int width, int height, int format);

void frame_exit(AVFrame *Frame);

#endif