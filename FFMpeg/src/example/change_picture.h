

#ifndef __CHANGE_PICTURE_H__
#define __CHANGE_PICTURE_H__
#include <stdint.h>

/**
 * @brief 亮度
 * 
 */
uint8_t *changePictureLuminance(char *sourcePath, float luminace);


/**
 * @brief 对比度
 */
uint8_t *changePictureConstrast(char *sourcePath, float constrast);


/**
 * @brief 饱和度
 */
uint8_t *changePictureSaturation(char *sourcePath, float saturation);

#if 0
/**
 * @brief 色温
 */
uint8_t *changePictureTemperature(char *sourcePath, float temperature);

/**
 * @brief 亮度
 */
uint8_t *changePictureBrightPart(char *sourcePath, float brightScale);

/**
 * @brief 暗度
 */
uint8_t *changePictureDarkPart(char *sourcePath, float darkScale);

uint8_t *changeBlackAndWhite(char *sourcePath, float scaleValue);
#endif


/**
 * @brief 锐度
 */
uint8_t *changePictureSharpness(char *sourcePath, float sharpness);


#endif