
#ifndef __PERS_FFMPEG_H__
#define __PERS_FFMPEG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/mathematics.h"

#include "picture_filter.h"
#ifdef __cplusplus
};
#endif


typedef  int (*codec_callback)(void *, void *, int len);



typedef struct 
{
	uint8_t         *buffer;
	AVFrame         *frame;
	AVPacket        *packet;
	AVIOContext     *avio;	
	AVFormatContext *fmt_ctx;
	int              channels;
	int              channel_layout;
	int              width;
	int              height;
	enum AVPixelFormat pix_fmt;
	
}codec_io_context_t;



typedef struct 
{
	codec_io_context_t in_context;
	codec_io_context_t out_context;
	picture_filter_t   filter;
}codec_context_t;


int picture_codec_process(codec_context_t  *codec);

int picture_codec_init(codec_context_t  *codec, void *args, codec_callback ReadFun, codec_callback WriteFun, const char *filter_descr);

int picture_codec_exit(codec_context_t  *codec);


#endif 

