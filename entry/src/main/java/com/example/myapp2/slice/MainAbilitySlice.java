package com.example.myapp2.slice;

import com.example.myapp2.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.PositionLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.ComponentContainer.LayoutConfig;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.*;

public class MainAbilitySlice extends AbilitySlice {
    // Load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("hello");
    }

    private PositionLayout myLayout = new PositionLayout(this);

    Image image;

    private String imagePath = "data/local/tmp/hiai/test.jpg";
    private String fileName = "test.jpg";

    public native byte[] changePictureLuminance(byte[] sourcePath, float luminace);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        File testFile = new File(getCacheDir(), fileName);
        try {
            Resource resource = getContext().getResourceManager().getResource(ResourceTable.Media_test);
            FileOutputStream fileOutputStream = new FileOutputStream(testFile);
            byte[] bytes = new byte[1024];
            while (resource.read(bytes) != -1) {
                fileOutputStream.write(bytes);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        image = (Image) findComponentById(ResourceTable.Id_image_test);
        ImageSource imageSource = ImageSource.create(imagePath, null);
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
        image.setPixelMap(pixelMap);

        findComponentById(ResourceTable.Id_button_change_luminance).setClickedListener(component -> {
            byte[] source = null;
            try {
                FileInputStream fileInputStream = new FileInputStream(new File(imagePath));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = fileInputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer);
                }
                fileInputStream.close();
                byteArrayOutputStream.flush();
                source = byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] picatureData = changePictureLuminance(source, -2f);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(picatureData);
            ImageSource imageSource1 = ImageSource.create(inputStream, null);
            ImageSource.DecodingOptions decodingOptions1 = new ImageSource.DecodingOptions();
            PixelMap pixelMap1 = imageSource1.createPixelmap(decodingOptions1);
            image.setPixelMap(pixelMap1);
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

}
