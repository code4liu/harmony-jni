
#ifndef __PICTURE_FILTER_H__
#define __PICTURE_FILTER_H__

#define __STDC_CONSTANT_MACROS
#ifdef __cplusplus
extern "C"
{
#endif

#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>

#ifdef __cplusplus
};
#endif

 typedef struct 
 {
    AVFilterGraph   *filter_graph;
	AVFilterContext *outFilterCtx;
	AVFilterContext *inFilterCtx;
	AVFilterInOut   *outputs;
	AVFilterInOut   *inputs;
 }picture_filter_t;


extern int picture_filter(picture_filter_t *fil, AVFrame *inFrame, AVFrame *outFrame);

extern int picture_filter_init(picture_filter_t *fil,
		const char *filter_descr, int width, 
		int height, enum AVPixelFormat format);


extern void picture_filter_exit(picture_filter_t *filter);

#endif
