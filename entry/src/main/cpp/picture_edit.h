
#ifndef __PICTURE_EDIT_H__
#define __PICTURE_EDIT_H__

#include "picture_codec.h"

#define  DEFAULT_PICTURE_SIZE  65536

typedef struct 
{
	uint8_t *out_data;
	uint32_t out_length;
	uint32_t out_max;

	uint8_t *in_data;
	uint32_t in_length;
	uint32_t in_max;
	codec_context_t ffmpeg_context;
}picture_edit_t;




/**
 * @brief 亮度
 * 	Luminance: 指的是投射在固定方向和面积上面的发光强度，发光强度是一个可测量的属性
 *  Brightness: 亮度是光的主观属性，显示器从暗到亮之间可以调节成不同程度等级的亮度，不能通过测量来客观评估（但可以说成比例，如50%的亮度）。
 * 
 * eq=brightness 效果和 hue=b=%f 一致
 */
uint8_t *changePictureLuminance(char *sourcePath, float luminace);


/**
 * @brief 对比度
 * @details
 * 		contrast 	是对比度
 * 		brightness 	是明亮 可以是小数
 * 
 */
uint8_t *changePictureConstrast(char *sourcePath, float constrast);

/**
 * @brief 饱和度
 */
uint8_t *changePictureSaturation(char *sourcePath, float saturation);


/**
 * @brief 色温
 * @details 条件 红蓝两色，负数调节红色，正数数添加蓝色
 * 			每种颜色的范围是 [-1.0 1.0]
 */
uint8_t *changePictureTemperature(char *sourcePath, float temperature);

/**
 * @brief  亮部: 在 Pshotoshop 中就是阴影和高光
 * @details 每个值的取值范围 [-1.0 1.0]
 */
uint8_t *changePictureBrightPart(char *sourcePath, float brightScale);

/**
 * @brief 暗部
 */
uint8_t *changePictureDarkPart(char *sourcePath, float darkScale);

/**
 * @brief 黑白
 * @details 彩色转换黑白
 *		y 参数调整亮度		    [0-256]
 *		u 参数调整蓝色平衡  	[0-256]
 *		v 参数调整红色平衡 		[0-256]
 */
uint8_t *changeBlackAndWhite(char *sourcePath, float scaleValue);


/**
 * @brief 锐度
 */
uint8_t *changePictureSharpness(char *sourcePath, float sharpness);

#endif